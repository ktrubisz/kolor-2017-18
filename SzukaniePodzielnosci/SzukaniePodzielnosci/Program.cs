﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzukaniePodzielnosci
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.zadanie domowe, sprawdzenie czy podana liczba jest liczbą pierwsz
            //2.zadanie domowe, wypisanie na ekran wszystkich dzielników podanej liczby

            Console.Write("Podaj  liczbe ");
            var liczba = Convert.ToInt16(Console.ReadLine());           

            //i = i + 1 to jest to samo co i++

            for (int i = 0; i <= liczba; i++)
            {
                //if(i % 2 == 0 && i % 3 == 0) Console.WriteLine(i);

                //uzupełnienie informacji http://www.centrumxp.pl/dotNet/391,12-Inne-operatory-logiczne.aspx

                if (i % 2 == 0 || i % 3 == 0) Console.WriteLine(i);

                // x && y  -> jeden i drugi warunek musi byc spelniony
                // x || y -> jeden lub drugi warunek musi być spelniony
            }

            Console.ReadLine();
        }
    }
}
