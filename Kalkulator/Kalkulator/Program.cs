﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator
{
    class Program
    {
        static void Main(string[] args)
        {
            //zadanie domowe
            //zrobić tak, żeby liczyć pierwiastek i potęge bez podawania drugiej liczby

            Console.WriteLine("Witaj w kalkulatorze");

            Console.Write("Podaj pierwsza liczbe: ");
            double liczba1 = Convert.ToDouble(Console.ReadLine());
            //var liczba1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Podaj znak dzialania (p - pierwiastek): ");
            char znak = Convert.ToChar(Console.ReadLine());

            Console.Write("Podaj druga liczbe: ");
            double liczba2 = Convert.ToDouble(Console.ReadLine());
 

            //if (znak == '+')
            //{
            //    Console.WriteLine($"{liczba1} + {liczba2} = {liczba1 + liczba2}");
            //}
            //else
            //{
            //    Console.WriteLine("Nie obslugujemy takiego znaku");
            //}

            switch (znak)
            {
                case '+':
                    Console.WriteLine($"{liczba1} + {liczba2} = {liczba1 + liczba2}");
                    break;
                case '-':
                    Console.WriteLine($"{liczba1} - {liczba2} = {liczba1 - liczba2}");
                    break;
                case '*':
                    Console.WriteLine($"{liczba1} * {liczba2} = {liczba1 * liczba2}");
                    break;
                case '/':
                    if(liczba2 == 0) Console.WriteLine("Nie dzielimy przez zero!");
                    else Console.WriteLine($"{liczba1} - {liczba2} = {liczba1 - liczba2}");
                    break;
                case 'p':
                    Console.WriteLine($"Pierwiastek z {liczba1} to {Math.Sqrt(liczba1)}");
                    break;
                default:
                    Console.WriteLine("Nie obslugujemy takiego znaku");
                    break;
            }
            Console.ReadLine();
        }
    }
}
