﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SuperKalkulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private double _value;
        private char _type;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddValue(string digit)
        {
            if (ResultTextBox.Text == "0") ResultTextBox.Text = digit;
            else ResultTextBox.Text += digit;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddValue("9");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AddValue("8");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            AddValue("7");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ResultTextBox.Text = "0";
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            _value = Convert.ToDouble(ResultTextBox.Text);
            ResultTextBox.Text = "0";
            _type = '+';
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            var oldValue = _value;
            var newValue = Convert.ToDouble(ResultTextBox.Text);

            double result = 0;
            //var result2 = 0.0;

            switch (_type)
            {
                case '+': result = oldValue + newValue;
                    break;

            }

            ResultTextBox.Text = result.ToString();
        }
    }
}
