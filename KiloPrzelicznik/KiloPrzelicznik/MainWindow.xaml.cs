﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KiloPrzelicznik
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CalBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var textValue = ValueTextBox.Text;
                textValue = textValue.Replace('.', ',');
                if (textValue == "") throw new Exception("Nie podałeś liczby");

                var value = Convert.ToDouble(textValue);
                var result = value * 10;
                //MessageBox.Show(result.ToString());
                MessageBox.Show($"Cena za kg to {Math.Round(result, 2)}");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool canChange = false;

        private void ValueTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var indexOstatniegoZnaku = ValueTextBox.Text.Length - 1;
            var ostatniZnak = ValueTextBox.Text[indexOstatniegoZnaku];
            var textString = ostatniZnak.ToString();

            try
            {
                var liczbe = Convert.ToInt16(textString);
            }
            catch (Exception ex)
            {
                ValueTextBox.Text = ValueTextBox.Text.Remove(indexOstatniegoZnaku,1);
            }
        }
    }
}
